import React from 'react';
import {ReactDom,render} from 'react-dom';
import Home from "./app/pages/home.jsx";
import YoutubeVideoPage from "./app/pages/youtubeVideoPage.jsx";
import cmsHomePage from "./app/pages/cmsHomePage.jsx";
import addVideo from "./app/pages/addVideo.jsx";
import updateVideos from "./app/pages/updateVideos.jsx";
import siteScraper from "./app/pages/siteScraper.jsx";
import addSite from "./app/pages/addSite.jsx";
import { Router, Route, Link, browserHistory, hashHistory} from 'react-router';

// ReactDom.render(<YoutubeVedio videoId={'5FIPIuw5KXY'}/>,document.getElementById("app"));
render((
  <Router history={hashHistory}>
	  <Route path="/" component={Home}/>
	  <Route path="/youtubeVedio/:videoId" component={YoutubeVideoPage}/>
	  <Route path = "/cms" component={cmsHomePage}>
	  	  <Route path = "/cms/addVideo" component={addVideo}/>
	  	  <Route path = "/cms/updateVideo" component={updateVideos}/>
	  	  <Route path = "/cms/site-scraper" component={siteScraper}/>
	  	  <Route path = "/cms/addSite" component={addSite}/>
	  </Route>
  </Router>
), document.getElementById('app'));