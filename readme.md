###1. setup the developping environment.###

```
npm install
bower install
```

###2. how to test.###

we use mocha + enzyme + chai to test the componnents. you can find nice tutorial [here](https://semaphoreci.com/community/tutorials/testing-react-components-with-enzyme-and-mocha, "mocha").

to create spy, you can use sinon. here is a example:

```
let sinon = require("sinon");
let cb = sinon.spy();

cb();

expect(cb.called).to.equal(true);
```

you can find good tutorial [here](http://sinonjs.org/docs/, "sinon").


To run the test you can excute the following command:

```
npm test
```

here is a example of test in ./test/searbox.sepc.js:

```
import React from 'react';
import { mount, shallow } from 'enzyme';
import {expect} from 'chai';
import SearchBox from "../app/components/SearchBox";

describe('<SearchBox/>', function () {
  let sinon = require("sinon");
  let cb = sinon.spy();

  const wrapper = shallow(<SearchBox cb={cb}/>);

  
  it('should have text input', function () {
    expect(wrapper.find('input')).to.have.length(1);
  });

  it("should have submit button", function() {
  	expect(wrapper.find("button")).to.have.length(1);
  });

  it("when button click, call back function shoudl be called", function() {
  	expect(cb.called).to.equal(false);
  	wrapper.find('form').simulate('submit');
  	expect(cb.calledWith("")).to.equal(true);
  });
});
```

###3. Access the site.###

1.start the webserver:

```
node server.js
```


2.open the url: http://127.0.0.1:8080/public/ in your browser, you shoudl be able see something.

