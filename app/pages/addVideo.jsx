import React from 'react';
import ReactDom from 'react-dom';
import Menu from "../components/menu.jsx";
import FileUploader from "../components/fileUploader.jsx";
import CategoryManagement from "../components/categoryManagement.jsx"
import OverlayProgress from "../components/overlayProgress.jsx"

export default class addVideo extends React.Component {
	constructor(props, context) {
		super(props);
		this.state = {
			categories: [
			],

			compeleted: 0,
			isLoading: false,

			videoTitleError: false,
			videoDescriptionError: false,
			videoFileError: false,
			videoImgError: false,
			categoryError: false
		};

		this.domHelper = require("../helpers/dom.js");
		this.videoUpload = require("../services/videoUpload.js");
		this.classNames = require('classnames');
		this.fileHelper = require("../helpers/fileHelper.js");
		this.categoryService = require("../services/category.js");
		this.initCategories();
	}

	emulateUpload() {
		if (this.state.compeleted < 100) {
			this.setState({compeleted: this.state.compeleted += 1});
		}

		setTimeout(() => {this.emulateUpload()}, 100);
	}

	initCategories() {
		this.categoryService.getCategories((categories) => {
			console.info("--- categories cb ---", categories);
			
			this.setState({categories: categories});
		})
	}

	showSectionAddNew() {
		jQuery(this.refs.newCategoryContainer).toggleClass("hide");
	}

	onCanvasDataChange(data) {
		console.info("--- value of data --", data);
		var data = data.split(",")[1];
		this.canvasData = data;
	}

	componentDidMount() {
		this.bindSubmitEvent();
		// this.emulateUpload();
	}

	validateForm() {
		if (this.refs.videoTitle.value == "") {
			this.setState({videoTitleError: true})
			return false;
		}

		if (this.refs.videoDescription.value == "") {
			this.setState({videoDescriptionError: true});
			return false;
		}

		if (this.refs.videoFile.value == "") {
			this.setState({videoFileError: true});
			return false;
		}

		if (this.fileHelper.getFileExtension(this.refs.videoFile.value) != "mp4") {
			this.setState({videoFileError: true});
			return false;
		}

		if (!this.canvasData) {
			this.setState({videoImgError: true});
			return false;
		}

		if (!jQuery("[name=videoCategory]:checked").length) {
			this.setState({categoryError: true});
			return false;
		}

		return true;
	}

	getFormData() {
		var formData = new FormData();
		formData.append("videoTitle", this.refs.videoTitle.value);
		formData.append("videoDescription", this.refs.videoDescription.value);
		formData.append("videoFile", this.refs.videoFile.files[0]);
		formData.append("thumbnailFile", this.canvasData);
		formData.append("categoryId", jQuery("[name=videoCategory]:checked").val());

		console.info("value of formData is:", formData);

		return formData;
	}

	categoryChange(result) {
		if (result.success) {
			this.initCategories();
		}else{
			alert("udpate category failed");
		}
	}

	bindSubmitEvent() {
		this.refs.videoForm.onsubmit = (e) => {
			e.preventDefault();

			if(!this.validateForm()) {
				return false;
			}
			
			let data = this.getFormData();
			this.setState({isLoading: true});

			this.videoUpload.uploadNewVedio(data, this.uploadOnProgress.bind(this)).then((result)=> {
				console.info("-- upload result --", result);
				alert("video has been uploaded successfully!");
				this.setState({isLoading: false});
			}).catch((error) => {
				console.info("-- upload error ---", error);
				alert("video uploaded failed: " + error);
				this.setState({isLoading: false});
			});
		}
	}

	uploadOnProgress(oEvent) {
		let percent = Math.round(oEvent.loaded / oEvent.total * 100);
		console.info("value of percent:",percent);
		this.setState({compeleted: percent});
	}

	setFormState(e) {
		var key = jQuery(e.target).attr('data');
		let obj = {};
		obj[key] = false;
		this.setState(obj);
	}

	render() {
		let basicClassNames = ['form-control'];
		let videoTitleClass = this.classNames(...basicClassNames, {error: this.state.videoTitleError});
		let descriptionClass = this.classNames(...basicClassNames, {error: this.state.videoDescriptionError});
		let videoFileClass = this.classNames(...basicClassNames, {error: this.state.videoFileError});
		let videoImgClass = this.classNames('col-sm-10', {error: this.state.videoImgError});
		let categoryClass = this.classNames('col-sm-10', 'new-catetory-wrappe', {error: this.state.categoryError});

		let overlayDiv = this.state.isLoading ? 
			(<OverlayProgress percentage={this.state.compeleted} className='hide'/>) :
			(<div></div>);

		return (
			<div>
				{overlayDiv}
				<form className="form-horizontal" ref="videoForm">
					<div className="form-group">
						<label className="col-sm-2 control-label">
							Vedio Title
						</label>
						<div className="col-sm-10">
							<input type="text" className={videoTitleClass} placeholder="video title" name="videoTitle" id="videoTitle" ref="videoTitle" onFocus={this.setFormState.bind(this)} data='videoTitleError'/>
						</div>
					</div>

					<div className="form-group">
						<label className="col-sm-2 control-label">
							Vedio description
						</label>
						<div className="col-sm-10">
							<textarea className={descriptionClass} placeholder="video description" name="videoDescription" id="videoDescription" ref="videoDescription" onFocus={this.setFormState.bind(this)} data="videoDescriptionError"></textarea>							
						</div>
					</div>

					<div className="form-group">
						<label className="col-sm-2 control-label">
							chose video to upload
						</label>
						<div className="col-sm-10">
							<input type="file" name="videoFile" className={videoFileClass} id="videoFile" ref="videoFile" onClick={this.setFormState.bind(this)} data="videoFileError"/>
						</div>
					</div>

					<div className="form-group">
						<label className="col-sm-2 control-label">
							chose images thumbnail to upload (120 * 90)
						</label>
						<div className={videoImgClass} data="videoImgError" onClick={this.setFormState.bind(this)}>
							<FileUploader name="thumbnailFile" onCanvasDataChange={this.onCanvasDataChange.bind(this)}/>
						</div>
					</div>

					<div className="form-group">
						<label className="col-sm-2 control-label">
							chose category
						</label>
						<div className={categoryClass} data="categoryError">
						     <Menu wichCheckbox='true' checkboxName="videoCategory" data={this.state.categories}/>
						     <a href="javascript:void(0)" onClick={this.showSectionAddNew.bind(this)}>+ Create New Cateory</a>
						     <div className="form-group hide" ref="newCategoryContainer">
						     	<CategoryManagement data={this.state.categories} categoryChange={this.categoryChange.bind(this)}/>
						     </div>
						</div>
					</div>

					<div className="form-group">
					    <label className="col-sm-2 control-label">
							<input type="submit" value="Save" className="btn btn-primary"/>
						</label>
						<div className="col-sm-10"></div>
					</div>

				</form>
			</div>
		);
	}
}
