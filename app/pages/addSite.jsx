import React from 'react';
import ReactDom from 'react-dom';
import {postWebsiteInfo} from '../services/scraper';

export default class addSite extends React.Component {
  constructor(props) {
    	super(props);

    	this.handleWebsiteInfo = this.handleWebsiteInfo.bind(this);
    	this.handleWebsiteInfoSave = this.handleWebsiteInfoSave.bind(this);

    	this.websiteInfo = {
			site:'', 
			video: { videoUrl: '', videoPageClass: '', articleVideoClass: '',  videoPagesScraped: ''}, 
			audio: { audioUrl: '', audioPageClass: '', articleAudioClass: '', audioPagesScraped:'' }
		}

	}

	handleWebsiteInfo(){
		this.websiteInfo = {
			site: ReactDom.findDOMNode(this.refs.site).value.trim(), 
			video: { 	videoUrl: ReactDom.findDOMNode(this.refs.videoUrl).value.trim(), 
						videoPageClass: '.' + ReactDom.findDOMNode(this.refs.videoPageClass).value.trim(), 
						articleVideoClass: '.' + ReactDom.findDOMNode(this.refs.articleVideoClass).value.trim(),  
						videoPagesScraped: ReactDom.findDOMNode(this.refs.videoPagesScraped).value.trim()
					}, 
			audio: { 	audioUrl: ReactDom.findDOMNode(this.refs.audioUrl).value.trim(), 
						audioPageClass: '.' + ReactDom.findDOMNode(this.refs.audioPageClass).value.trim(), 
						articleAudioClass: '.' + ReactDom.findDOMNode(this.refs.articleAudioClass).value.trim(), 
						audioPagesScraped: ReactDom.findDOMNode(this.refs.audioPagesScraped).value.trim() 
					}
		}
	}

	handleWebsiteInfoSave(){
		console.log("Website Info", this.websiteInfo);	
		postWebsiteInfo(this.websiteInfo, (res) =>{
			console.log("websiteInfo Saved!!", res);
		})
	}	

	render(){
		return(
			<div>
				<h3> Website Scraping Info </h3>
				<form className="form-horizontal">
					<div className="form-group">
		     			<div className="col-sm-12">
		     				<div className="col-sm-3 control-label">
			     				<label htmlFor="siteName"> Site </label>
		     				</div>
		     				<div className="col-sm-3">
				     			<input 
						          type="text"
						          name="siteName"
						          placeholder="Site"
						          ref="site"
						          onChange={this.handleWebsiteInfo}
				        		/> http://jaguda.com/
			        		</div>
		        		</div> 
		     		</div>

		     		<div className="form-group">
		     			<div className="col-sm-12">
		     				<div className="col-sm-3 control-label">
			     				<label htmlFor="videoUrl"> Video Url </label>
		     				</div>
		     				<div className="col-sm-3">
				     			<input 
						          type="text"
						          placeholder="Videos Link"
						          ref="videoUrl"	
						          onChange={this.handleWebsiteInfo}
				        		/> http://jaguda.com/videos/
			        		</div>
			    
			        		<div className="col-sm-3 control-label">
			     				<label> Video Page Class </label>
		     				</div>
			        		<div className="col-sm-3">
				     			<input 
						          type="text"
						          placeholder="Video Class"
						          ref="videoPageClass"	
						          onChange={this.handleWebsiteInfo}
				        		/> td-module-thumb
			        		</div>
		        		</div> 
		     		</div>

		     		<div className="form-group">
		     			<div className="col-sm-12">
		     				<div className="col-sm-3 control-label">
			     				<label htmlFor="audioUrl"> Audio Url </label>
		     				</div>
		     				<div className="col-sm-3">
				     			<input 
						          type="text"
						          placeholder="Audio Link"
						          ref="audioUrl"	
						          onChange={this.handleWebsiteInfo}
				        		/> http://jaguda.com/audio/
			        		</div>

			        		<div className="col-sm-3 control-label">
			     				<label> Audio Page Class </label>
		     				</div>
			        		<div className="col-sm-3">
				     			<input 
						          type="text"
						          placeholder="Audio Class"
						          ref="audioPageClass"	
						          onChange={this.handleWebsiteInfo}
				        		/> td-module-thumb
			        		</div>
		        		</div> 
		     		</div>


		     		<div className="form-group">
		     			<div className="col-sm-12">
		     				<div className="col-sm-3 control-label">
			     				<label> Article Video Class </label>
		     				</div>
		     				<div className="col-sm-3">
				     			<input 
						          type="text"
						          placeholder="Article Video Class"
						          ref="articleVideoClass"	
						          onChange={this.handleWebsiteInfo}
				        		/> 
			        		</div>

			        		<div className="col-sm-3 control-label">
			     				<label> Article Audio Class </label>
		     				</div>
			        		<div className="col-sm-3">
				     			<input 
						          type="text"
						          placeholder="Article Audio Class"
						          ref="articleAudioClass"	
						          onChange={this.handleWebsiteInfo}
				        		/> 
			        		</div>
		        		</div> 
		     		</div>

		     		<div className="form-group">
		     			<div className="col-sm-12">
		     				<div className="col-sm-3 control-label">
			     				<label> Video Pages Scraped </label>
		     				</div>
		     				<div className="col-sm-3">
				     			<input 
						          type="text"
						          placeholder="Video Pages Scraped"
						          ref="videoPagesScraped"	
						          onChange={this.handleWebsiteInfo}
				        		/> 
			        		</div>

			        		<div className="col-sm-3 control-label">
			     				<label> Audio Pages Scraped </label>
		     				</div>
			        		<div className="col-sm-3">
				     			<input 
						          type="text"
						          placeholder="Audio Pages Scraped"
						          ref="audioPagesScraped"	
						          onChange={this.handleWebsiteInfo}
				        		/> 
			        		</div>
		        		</div> 
		     		</div>
		     		<div className="form-group">
			     		<div className="col-sm-12">
			     			<div className="col-sm-6">
			     				<button type="button" className="btn btn-primary" onClick={this.handleWebsiteInfoSave}>Save</button>
			     			</div>
			     		</div>
		     		</div>	
				</form>
			</div>
		)	
	}
}