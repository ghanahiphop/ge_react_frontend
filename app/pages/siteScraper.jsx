import React from 'react';
import ReactDom from 'react-dom';
import { getUrlsToScrape, saveScrapedUrls } from '../services/scraper'
import Links from '../components/links.jsx';
import Loader from "../components/loader.jsx";

export default class siteScraper extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleFormValues = this.handleFormValues.bind(this);
    this.handleClear = this.handleClear.bind(this);
    this.handleSaveToDb = this.handleSaveToDb.bind(this);
    this.formValues = {site: '', contentClass:'', elements:'', pages: 0, paginataionClass: ''};
    this.state = {contents: {}, isLoading: false, isSaved: false};
  }
  
  handleClear(){
    this.setState({ contents: {} })
  }

  handleSubmit(){
  	this.handleClear();
     this.setState({isLoading: true});
  	getUrlsToScrape(this.formValues, (content) => {
      this.setState({isLoading: false});
      this.setState({ contents: content})
  	});
  }

  handleFormValues(){
  	this.formValues = {
      site: ReactDom.findDOMNode(this.refs.site).value.trim(),
      contentClass: ReactDom.findDOMNode(this.refs.contentClass).value,
      elements: [ReactDom.findDOMNode(this.refs.elements).value],
      pages: ReactDom.findDOMNode(this.refs.pages).value,
      paginataionClass: ReactDom.findDOMNode(this.refs.paginataionClass).value

    }
  }

  handleSaveToDb(){
    this.setState({ isLoading: true});
    saveScrapedUrls(this.state.contents.urls, (response) => {
      if(response.success)
        this.setState({contents:{}, isLoading: false, isSaved: response.success});
    })
  }

  render() {
    let resultTag = "";    
     if (this.state.isLoading) {
      resultTag = (<Loader/>);
    }
    else{
      resultTag = (<Links contents={this.state.contents} saveToDb={this.handleSaveToDb} isSaved={this.state.isSaved} />);
    }

    return (
     <div>
     <h1> Get Content </h1>

     	<form className="form-horizontal">
     		<div className="form-group">
     			<div className="col-sm-12">
	     			<input 
			          type="text"
			          placeholder="Site"
			          ref="site"
			          onChange={this.handleFormValues}
	        		/> http://jaguda.com/videos/
        		</div> 
     		</div>

     		<div className="form-group">
     		<div className="col-sm-12">
	     			<input 
			          type="text"
			          placeholder="Content to scrape"
			          ref= "contentClass"
			          onChange={this.handleFormValues}
	        		/> .td-module-thumb
        		</div> 
     		</div>

     		<div className="form-group">
     		<div className="col-sm-12">
	     			<input 
			          type="text"
			          placeholder="Elements to obtain"
			          ref= "elements"
			          onChange={this.handleFormValues}
	        		/> * Not required will be used later.
        		</div> 
     		</div>

     		<div className="form-group">
     		<div className="col-sm-12">
	     			<input 
			          type="text"
			          placeholder="No of Pages"
			          ref= "pages"
			          onChange={this.handleFormValues}
	        		/> 1
        		</div> 

     		</div>
   			
   			<div className="form-group">
   			<div className="col-sm-12">
	     			<input 
			          type="text"
			          placeholder="Pagination Class"
			          ref="paginataionClass"
			          onChange={this.handleFormValues}
	        		/> .td-pb-padding-side a@href
        		</div>
     		</div>

     		<div className="form-group">
            <div className="col-sm-12"> 
       			  <div className="col-sm-3"> 
                <input type="button" value="Scrape Urls" onClick={this.handleSubmit} />
              </div>
              <div className="col-sm-3">
                <input type="button" value="Clear" onClick={this.handleClear}/>
              </div>
              <div className="col-sm-3"></div>
        </div>      
     		</div>
     	</form>

      <h5> Urls : {this.formValues.site} </h5> 
      {resultTag}

    </div>
    );
  }
}
