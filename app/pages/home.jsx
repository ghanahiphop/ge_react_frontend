import React from 'react';
import ReactDom from 'react-dom';
import SearchBox from "../components/SearchBox.jsx";
import VideoList from "../components/videoList.jsx";
import YoutubeVedio from "../components/youtubeVideo.jsx";
import LoadMore from "../components/loadMore.jsx";
import Loader from "../components/loader.jsx";


export default class Home extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			data: [],
			isLoading: false,
			loadMore: false
		}
		this.videoList = require("../services/videoList.js");
	}

	componentDidMount() {
	}

	search(q) {
		this.setState({isLoading: true});
		this.videoList.getSearchResult(q).then((result) => {
			this.updateData(result, q);
			this.setState({isLoading: false});
		}).catch((errr) => {
			console.errr("search error:", error);
			this.setState({isLoading: false});
		});
	}

	setPageToken(data, q) {
		if (data.nextPageToken) {
			this.pageInfo = {
				searchKey: q,
				nextPageToken: data.nextPageToken
			}

			this.setState({loadMore: true})
		}else{
			this.pageInfo = null;
			this.setState({loadMore: false})
		}
	}

	updateData(result, q, loadMore) {
		let data = JSON.parse(result);
		let parsedData = this.videoList.parseApiData(data);
		
		if (loadMore) {
			parsedData = this.state.data.concat(parsedData);
		}

		this.setState({data: parsedData}, () => {});
		this.setPageToken(data, q);
	}

	loadMore() {
		if (this.pageInfo) {
			this.setState({isLoading: true})
			this.videoList.loadMore(this.pageInfo).then((result) => {
				this.updateData(result, this.pageInfo.searchKey, true);
				this.setState({isLoading: false});
			}).catch((error) => {
				console.error("loadMore", error);
				this.setState({isLoading: false});
			})
		}
	}

	render() {
		let display = this.state.loadMore ? "" : "hide";
		let searchResult = "";		
		
		if (this.state.isLoading && !this.state.loadMore) {
			searchResult = (<Loader/>);
		}else{
			searchResult = (<VideoList data={this.state.data}/>);
		}

		return (
			<div className="container padding-top-100">
				<div className="row">
					<SearchBox cb={this.search.bind(this)}/>
				</div>
				
				<div id="searchResult">
					{searchResult}
				</div>

				<LoadMore cb={this.loadMore.bind(this)} display={this.state.loadMore} loading={this.state.isLoading}/>
			</div>
		);
	}
}
