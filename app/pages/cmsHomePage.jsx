import React from 'react';
import ReactDom from 'react-dom';
import {Navigation} from 'react-router';
import LoginForm from "../components/loginForm.jsx";
import Menu from "../components/menu.jsx";

export default class cmsHomePage extends React.Component {
	constructor(props, context) {
		super(props);
		this.users = require("../services/users");
		this.state = {
			isLogin: this.users.isLogin(),
			loginError: '',
			menus: [
				{
					url:"",
					title:"Video Management",
					children: [
						{
							url: "/cms/addVideo",
							title: "Add New Video"
						},
						{
							url: "/cms/updateVideo",
							title: "Update Video"
						}
					]
				},
				{
					url:"/cms/userManagement",
					title:"User Management",
				},
				{
					url:"/cms/site-scraper",
					title:"Scrape Websites",
					children: [
						{
							url: "/cms/site-scraper",
							title: "Scrape Contents"
						},
						{
							url: "/cms/addSite",
							title: "Add New Site"
						}					
					]
				}

			]
		}
	}

	componentDidMount() {
		// this.refs.cmsContainer.style.height = window.innerHeight + "px";
	}

	handleLogin(data) {
		this.users.login(data).then((result) => {
			var response = JSON.parse(result);

			if (response.success) {
				this.users.setUserToken(response.token);
				this.setState({isLogin: this.users.isLogin()});
			}else{
				this.setState({loginError: ""});
				this.setState({loginError: response.msg});
			}

		}, (error) => {
			console.error("login:",error);
		});
	}

	handleLogout() {
		this.users.logout();
		this.setState({isLogin: this.users.isLogin()});
		this.setState({loginError: ""});
	}

	render() {
		var content = null;

		if (!this.state.isLogin) {
			content = (<LoginForm cb={this.handleLogin.bind(this)} errorMsg={this.state.loginError}/>);
		}else{
			content = (
				<div className="row row-wrapper">
					<div className="menu col-xs-5 col-sm-4 col-md-3	col-lg-3">
						<div className="site-title">Gana Media</div>
						<Menu data={this.state.menus}></Menu>
					</div>
					<div className="content-wrapper col-xs-7 col-sm-8 col-md-9 col-lg-9">
						<div className="header">
						   <span className="glyphicon glyphicon-log-out pull-right log-out" onClick={this.handleLogout.bind(this)}></span>
						   <div className="clearfix"></div>
						</div>
						<div className="content-area">
							{this.props.children}
						</div>
					</div>
				</div>
			);
		}

		return (
			<div className="container-fluid cms-container" ref="cmsContainer">
				{content}
			</div>
		);
	}
}
