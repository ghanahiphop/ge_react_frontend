import React from 'react';
import Modal from '../components/modal.jsx';

export default class updateVideos extends React.Component {
	constructor(props, context) {
		super(props);
		
		this.state = {
			videos: [],
			currentVideo: null
		}

		this.localVideos = require("../services/localVideos");
		this.pathHelper = require("../helpers/pathHelper");
		this.getVideoList();
	}

	getVideoList() {
		this.localVideos.getVideos().then((results) => {
			let parsedData = JSON.parse(results).data;
			this.setState({
				videos: parsedData
			})
		}).catch((e) => {
				
		});
	}

	handleClick(v) {
		let videoPath = this.pathHelper.buildPath("/getVedio/" + v.videoDetail.filename);
		this.refs.videoPlayer.pause();
		this.setState({currentVideo: videoPath});
		this.refs.videoPlayer.load();
		this.refs.videoPlayer.play();
		$("#videoPlayer").modal("show");
	}

	deleteVideo(id) {
		if(confirm("are you sure you want to delete this video?")) {
			this.localVideos.deleteVideo(id).then((results) => {
				if (results.success) {
					this.getVideoList();
				}
			}).catch((e) => {
				console.info("server error:", e);
				alert(e);
			});
		}
	}

	editVideo(id) {
		console.info("--- editVideo ---", id);
	}

	componentDidUpdate() {
		console.info("components did update");
		$('#videoPlayer').on('hidden.bs.modal', (e) => {
			this.refs.videoPlayer.pause();
		});
	}

	render() {
		let videos = this.state.videos.map((v, k) =>{
			return (
				<tr key={k}>
					<td>
						{v.title}
					</td>
					<td>
						{v.description}
					</td>
					<td>
						{v.category.title}
					</td>
					<td>
						<img src={"/images/" + v.thumbnailFile} onClick={() => this.handleClick(v)}/>
					</td>
					<td>
						<div className="btn-group" role="group">
							<div className="btn btn-info" onClick={() => this.editVideo(v._id)}>
								<span className="glyphicon glyphicon-edit"></span>
							</div>
							<div className="btn btn-danger" onClick={() => this.deleteVideo(v._id)}>
								<span className="glyphicon glyphicon-floppy-remove"></span>
							</div>
						</div>					
					</td>
				</tr>
			);
		});

		return (
		 	<div>
			 	<Modal modalId="videoPlayer" title="video player">
			 		<video controls name="media" width="400" ref="videoPlayer">
			 			<source src={this.state.currentVideo} type="video/mp4"/>
			 		</video>
			 	</Modal>
			 	<div className="table-responsieve">
					<table className="table table-bordered table-striped table-hover">
						<tbody>
							<tr>
								<th>video title</th>
								<th>video description</th>
								<th>video catetory</th>
								<th>video thumbnail</th>
								<th>actions</th>
							</tr>
							{videos}
						</tbody>
					</table>
				</div>
			</div>
		);
	}

}