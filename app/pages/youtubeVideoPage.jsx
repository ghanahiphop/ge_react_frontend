import React from 'react';
import ReactDom from 'react-dom';
import SearchBox from "../components/SearchBox.jsx";
import VideoList from "../components/videoList.jsx";
import YoutubeVideo from "../components/youtubeVideo.jsx";
import {Link} from 'react-router';


export default class YoutubeVideoPage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {data: []}
		this.api = require("../services/api.js");
		this.videoList = require("../services/videoList.js");
	}

	componentDidMount() {
		if (this.props.params && this.props.params.videoId) {
			this.updateList();
		}
	}

	updateList() {
		this.videoList.getRelatedVideos(this.props.params.videoId)
			.then((result) => {
				this.updateData(result);
			});
	}

	componentWillReceiveProps(nextProps) {
		this.updateList();
	}

	updateData(result, q, loadMore) {
		let data = JSON.parse(result);
		let parsedData = this.videoList.parseApiData(data);
		
		if (loadMore) {
			parsedData = this.state.data.concat(parsedData);
		}

		this.setState({data: parsedData}, () => {});
	}

	render() {
		return (
			<div className="container">
				<div className="row">
					<div className="col-md-9">
						<YoutubeVideo videoId={this.props.params.videoId}/>
					</div>
					<div className="col-md-3">
						<VideoList data={this.state.data}/>	
					</div>
				</div>
			</div>
		);
	}
}
