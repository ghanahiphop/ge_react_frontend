"use strict";

module.exports = {
	host: location.hostname,
	apiPath: "/search",
	port:8080,
}