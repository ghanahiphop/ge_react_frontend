"use strict";

let api = require("./api.js");
let pathHelper = require("../helpers/pathHelper.js")

module.exports = {
	getCategories: function(cb) {
		api.buildRequest(pathHelper.buildPathWithToken("category"), "GET").then((result) => {
			api.standResponse(result, (newResult) => {
				cb(this.serizeData(newResult.data));
			});
		}).catch(function(msg) {
			throw "error on fetching category";
		});
	},

	serizeData: function(data) {
		var that = this;

		var newArr = data.map(function(v, k) {
			var obj = {
				id: v._id,
				title: v.title
			}

			if (v.parentId) {
				obj.parentId = v.parentId;
			}
			
			return obj;
		});

		for(var i=0; i < newArr.length; i++) {
			var v = newArr[i];

			if (v.parentId) {
				var obj = that.getCategoryById(newArr, v.parentId);

				if (!obj) {
					continue;
				}

				if (!obj.children) {
					 obj.children = [];
				}

				obj.children.push(v);
				newArr.splice(i, 1);
				i--;
			}
		}

		return newArr;
	},

	getCategoryById: function(data, checkedId) {
		var newArr = data.map((v)=>{
			if(v.id === checkedId) {
				return v;
			}else if(v.children && v.children.length > 0) {
				return this.getCategoryById(v.children, checkedId)
			}

			return null;
		}).filter((v) => {
			return !!v; 	
		});

		if (newArr.length > 0) {
			return newArr.pop();
		}

		return null;
	},

	updateCategory: function(newCategory, cb) {
		api.buildRequest(pathHelper.buildPathWithToken("category/update"), "POST", newCategory).then((result) => {
			api.standResponse(result, cb);
		});
	},

	deleteCategory: function(category, cb) {
		api.buildRequest(pathHelper.buildPathWithToken("category/delete/" + category.id), "DELETE").then((result) => {
			api.standResponse(result, cb);
		})
	},

	addCategory: function(categoryTitle, parentCategory, cb) {
		var data = {
			title: categoryTitle
		}

		if (parentCategory) {
			data.parentId = parentCategory.id;
		}

		api.buildRequest(pathHelper.buildPathWithToken("category/add"), "POST", data).then((result) => {
			api.standResponse(result, cb);
		})
	}
}