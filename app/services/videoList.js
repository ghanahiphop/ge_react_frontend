"use strict";

let api = require("./api.js");
let config = require("../config.js");

module.exports = {
	parseApiData: function(originalData) {
		let newData = [];

		originalData.items.map((item, index) => {
			let snippet = item.snippet;

			newData.push({
				img: snippet.thumbnails.default,
				title: snippet.title,
				publishedAt: snippet.publishedAt,
				id: item.id.videoId || item.id.channelId
			}); 
		});

		return newData;
	},

	getSearchResult: function(q) {
		return api.buildRequest(config.apiPath, "POST", {
			"q":q
		});
	},

	getRelatedVideos: function(videoId) {
		return api.buildRequest(config.apiPath + "/" + videoId, "GET");
	},

	loadMore: function(pageInfo) {
		return api.buildRequest(config.apiPath, "POST", {
			"q": pageInfo.searchKey,
			"action": "nextPageToken",
			"actionValue": pageInfo.nextPageToken
		});
	}
}