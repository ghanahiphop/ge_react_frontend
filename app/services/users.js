"use strict";

let api = require("./api.js");

module.exports = {
	isLogin: function() {
		if (!sessionStorage.getItem("token")) {
			return false;
		}

		return true;
	}, 

	login: function(data) {
		return api.buildRequest("/login", "POST", data);
	},

	logout: function() {
		sessionStorage.removeItem("token");
	},

	setUserToken: function(token) {
		sessionStorage.setItem("token", token);
	},

	getUserToken: function() {
		return sessionStorage.getItem("token");
	}
}