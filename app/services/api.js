"use strict";

let http = require("http");
let Q = require("q");
let _ = require("underscore")();
let config = require("../config.js");

module.exports = {
	buildRequest: function(path, method, data) {
		let deferred = Q.defer(),
			response = "",
			basicOptions = {
				host: config.host,
				port: config.port,
				path: path,
				method: method,
				headers: {
					"Cache-Control": "no-cache"
				}
			},
			postReq = http.request(basicOptions, function(res) {
				res.on('data', function (chunk) {
					  response += chunk;
				});

				res.on('end', function(result) {
					   deferred.resolve(response);
				});

				res.on("error", function(error) {
					   deferred.reject(error);
				});
		  	});
		
		if (typeof data != "undefined") {			 
			 
			 try{
			 	data = JSON.stringify(data);
			 }catch(e){

			 }

			 postReq.write(data);
		};

		postReq.end();

		return deferred.promise;
	},

	http: function(method, path, data, onProgress) {
		let xhr = new XMLHttpRequest(),
			deferred = Q.defer();

		xhr.open(method, path, true);

		xhr.onload = function() {
			if (xhr.status === 200) {
				deferred.resolve(xhr.responseText);
			}else{
				deferred.reject(xhr.status);
			}
		}

		// xhr.onprogress = onProgress;
		xhr.upload.onprogress = onProgress;

		xhr.send(data);

		return deferred.promise;
	},

	standResponse: function(result, cb) {
		result = JSON.parse(result);

		if (!result.success) {
			console.error(result.msg);
			throw new Error(result.msg);
		}
		console.info("--- value of result ---", result);
		cb(result);
	}
}
