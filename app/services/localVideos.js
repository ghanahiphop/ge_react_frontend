"use strict";

let api = require("./api.js");
let pathHelper = require("../helpers/pathHelper.js")

module.exports = {
	getVideos: function() {
		return api.buildRequest("/getAllVideos");
	},

	deleteVideo: function(id) {
		return new Promise((resolve, reject) => {
			api.buildRequest(pathHelper.buildPathWithToken("deleteVideo/" + id), "DELETE").then((result) => {
				api.standResponse(result, (newResult) => {
					resolve(newResult);				
				});
			}).catch((msg) => {
				reject(msg);				
			});
		});
	}
}