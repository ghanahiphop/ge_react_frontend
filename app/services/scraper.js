"use strict";

let api = require("./api.js");
let pathHelper = require("../helpers/pathHelper.js")


export function getUrlsToScrape(data, cb){
	api.buildRequest(pathHelper.buildPathWithToken("scrape/getUrlsToScrape"), "POST", data).then((result) => {
			api.standResponse(result, cb);
		});
}

export function saveScrapedUrls(data, cb){
	api.buildRequest(pathHelper.buildPathWithToken("scrape/saveScrapedUrls"), "POST", data).then((result) => {
			api.standResponse(result, cb);
		});
}

export function postWebsiteInfo(data, cb){
	api.buildRequest(pathHelper.buildPathWithToken("scrape/postWebsiteInfo"), "POST", data).then((result) => {
			api.standResponse(result, cb);
		});
}