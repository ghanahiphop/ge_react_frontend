"use strict";

let api = require("./api.js");
let pathHelper = require("../helpers/pathHelper.js");

module.exports = {
	uploadNewVedio: function(data, onProgress) {
		return api.http("post", pathHelper.buildPathWithToken("admin/addVedio"), data, onProgress);
	}
}