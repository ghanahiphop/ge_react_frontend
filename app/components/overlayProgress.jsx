import React from 'react';
import className from 'classnames';

export default class OverlayProgress extends React.Component {
	constructor(props, context)	{
		super(props);
	}

	render() {
		let widthStyle = {"width": this.props.percentage + "%"};
		let rootClassName = className('progress-overlay', {'hide': this.props.percentage >= 100})
		
		return (
			<div className={rootClassName}>
				<div className="progress">
				    <div className="progress-bar" role="progressbar" aria-valuenow={this.props.percentage} aria-valuemin="0" aria-valuemax="100" style={widthStyle}>
				      <span className="sr-only">{this.props.percentage}% Complete</span>
				    </div>
				</div>
			</div>
		);
	}
}