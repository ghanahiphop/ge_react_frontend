import React from 'react';
import ReactDom from 'react-dom';
import {Link} from 'react-router';

let moment = require("moment");

export default class VideoThumbnail extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		let classes = "video-thumbnail";

		if (this.props.size) {
			classes += " " + this.props.size; 
		}

		let link = "/youtubeVedio/" + this.props.data.id;

		return (
			<div className={classes}>
				<div className="img_container">
					<img src={this.props.data.img.url} 
						width={this.props.data.img.width}
						height={this.props.data.img.height}/>
				</div>

				<div className="title">
					<Link to={link} className="users">
						{this.props.data.title}
					</Link>
				</div>
				<div className="time-line">
					{moment(new Date(this.props.data.publishedAt)).fromNow()}
				</div>
			</div>
		);
	}
}
