import React from 'react';
import ReactDom from 'react-dom';

export default class LoadMore extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		let classes = "row load-more";

		if (this.props.loading) {
			classes += " loading";
		}

		if (!this.props.display) {
			classes += " hide";
		}

		return (
			<div className={classes}>
				<div className="btn btn-primary" onClick={this.props.cb}>
					<span className="glyphicon glyphicon-refresh"></span>
					Load More
				</div>
			</div>
		);
	}
}