import React from 'react';
import ReactDom from 'react-dom';

export default class LoginForm extends React.Component {
	constructor(props) {
		super(props);
	}

	handleSubmit(e) {
		if (e && e.preventDefault) {
			e.preventDefault();
		}

		this.props.cb({
			username: this.refs.username.value,
			password: this.refs.password.value
		});
	}

	hideAlert() {
		this.refs.alert.style.display="none";
	}

	render() {
		let extraMsg = '';
		
		if (this.props.errorMsg && this.props.errorMsg!='') {
			extraMsg = (
				<div className="alert alert-danger alert-dismissible" ref="alert">
				  <button type="button" className="close" aria-label="Close" onClick={this.hideAlert.bind(this)}><span aria-hidden="true">&times;</span></button>
				  <strong>Warning!</strong> 
				  &nbsp;{this.props.errorMsg}
				</div>);
		}

		return (
			<div className="login-form">
			  <h3>Login to Your Account</h3>
			  <form onSubmit={this.handleSubmit.bind(this)}> 
			     {extraMsg}
			     
			     <div className="form-group">
				    <label>User Name</label>
				    <input type="text" className="form-control" id="username" placeholder="User Name" ref="username"/>
				 </div>
			     
			     <div className="form-group">
				    <label>Password</label>
				    <input type="password" className="form-control" id="password" placeholder="password" ref="password"/>
				 </div>

			     <div className="form-group">
					<button type="submit" className="btn btn-primary">Submit</button>  
				 </div>
			  </form>				
			</div>
		);
	}
}