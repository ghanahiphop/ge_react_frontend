import React from 'react';
import ReactDom from 'react-dom';

export default class YoutubeVideo extends React.Component {
	constructor(props) {
		super(props);
		
		let width = 640;
		
		this.ratio = 1.5;
		
		this.state = {
			width: width,
			height: width / this.ratio
		};
	}

	componentDidMount() {
		this.setVideoSize(this.refs.wrapper.offsetWidth);
		window.addEventListener('resize', this.handleResize.bind(this));
	}

	handleResize() {
		this.setVideoSize(this.refs.wrapper.offsetWidth);
	}

	setVideoSize(width) {
		this.setState({
			width: width,
			height: width / this.ratio
		});
	}

	render() {
		let src = "http://www.youtube.com/embed/" + this.props.videoId;
		
		return (
			<div className="row video-wrapper-container" ref="wrapper">
				<div className="video-wrapper">
					<iframe title="YouTube video player" 
						className="youtube-player" 
						type="text/html"
						src={src}
						width={this.state.width}
						height={this.state.height}
						frameBorder="0" 
						allowFullScreen/>
				</div>
			</div>
		);
	}
}



