import React from 'react';
import ReactDom from 'react-dom';

export default class FileUploader extends React.Component {
	constructor(props) {
		super(props);
		this.img = null;
		this.size = {
			width: 120,
			height: 90
		}

		this.fileHelper = require("../helpers/fileHelper.js");
	}

	showCrop() {
		let cropDom = "<div class='crop-wrapper'>" +
			"<div class='crop-area'></div>" +
			"</div>";

		jQuery(this.refs.preview).append(cropDom);
	}

	getMatrix(selector) {
		var obj = jQuery(selector);
		var transformMatrix = obj.css("-webkit-transform") ||
			obj.css("-moz-transform")    ||
			obj.css("-ms-transform")     ||
			obj.css("-o-transform")      ||
			obj.css("transform");

		var matrix = transformMatrix.replace(/[^0-9\-.,]/g, '').split(',');
		var x = matrix[12] || matrix[4] || 0;//translate x
		var y = matrix[13] || matrix[5] || 0;//translate y

		return {
			x: x,
			y: y
		}
	}

	bindDragEvents(size) {
		let cropDom = jQuery(this.refs.preview).find(".crop-wrapper");
		cropDom.css({"min-width":size.width + "px", "min-height":size.height + "px"});
		
		let cropArea = jQuery(this.refs.preview).find(".crop-area");
		let that = this;
		
		var obj = {
			mousedown: false,
			startPos: null,
			
			dragStart: function(e) {
				this.startPos = {
					x: e.offsetX, 
					y: e.offsetY
				}
			},

			drag: function(e) {
				var dis = {
					x: e.offsetX - this.startPos.x,
					y: e.offsetY - this.startPos.y
				}

				var nowPos = that.getMatrix(cropArea),
					newPos = {
						x: parseInt(nowPos.x) + parseInt(dis.x),
						y: parseInt(nowPos.y) + parseInt(dis.y)
					}
				
				cropArea.css({"transform":"translate(" + newPos.x + "px, " + newPos.y +"px)"});
			},

			dragEnd: function(e) {
				var newPos = that.getMatrix(cropArea);
				that.drawToCanvas(newPos, size);
			}
		}

		cropArea.bind("mousedown", (e) => {	
			obj.mousedown = true;	
			obj.dragStart(e);
		});

		cropArea.bind("mousemove", (e) => {	
			if (obj.mousedown) {
				obj.drag(e);
			}		
		});

		cropArea.bind("mouseup", (e) => {	
			if (obj.mousedown) {
				obj.dragEnd(e);
			}

			obj.mousedown = false;
		});

		jQuery("body").bind("mouseup", function() {
			obj.mousedown = false;
		})
	}

	drawToCanvas(newPos, size) {
		var canvas = this.refs.previewCrop,
			context = canvas.getContext('2d');

		context.clearRect(0,0, canvas.width, canvas.height);
		context.drawImage(this.img, newPos.x, newPos.y, size.width, size.height, 0, 0, size.width, size.height);
		this.props.onCanvasDataChange(canvas.toDataURL());
	}

	readerOnError(e) {
		console.info("error on reading file");
	}

	readerOnProgress(e) {
		console.info("onprogress:", e);
	}

	readerOnAbort(e) {
		console.info("reader abort", e);
	}

	initImage(e) {
		this.img = new Image();
		this.img.src = e.target.result;
		this.img.onload = () => {
			if (this.size.width != this.img.width && this.size.height != this.img.height && (this.img.width > this.size.width | this.img.height > this.size.height)) {
				this.showCrop({width: this.img.width, height: this.img.height});
				this.bindDragEvents(this.size);
				let cropArea = jQuery(this.refs.preview).find(".crop-area");
				var newPos = this.getMatrix(cropArea);
				this.drawToCanvas(newPos, this.size);
			}
		}

		this.imageSource = e.target.result;

		var imgHtml = "<img src='"+ e.target.result +"'/>";
		this.refs.preview.innerHTML = imgHtml;
	}

	thumnailChange(e){
		let fileRef = e.target.files[0];
		let reader = new FileReader();
		let allowedTypes = ["jpg", "png", "gif", "bmp"];
		let extension = this.fileHelper.getFileExtension(fileRef.name);
		
		if (allowedTypes.indexOf(extension) === -1 ) {
			return false;
		}


		reader.onload = (e) => {
			this.initImage(e);
		};

		reader.onerror = this.readerOnError;
		reader.onprogress = this.readerOnProgress;
		reader.onabort = this.readerOnAbort
		reader.readAsDataURL(fileRef);
	}

	render() {
		return (
			<div className="uploader">
				<input type="file" name={this.props.name} className="form-control" ref="thumbnailFile" onChange={this.thumnailChange.bind(this)}/>
				<div ref="preview" className="preview">
				</div>
				<canvas ref="previewCrop" width={this.size.width} height={this.size.height}></canvas>
			</div>
		)
	}
}