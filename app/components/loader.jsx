import React from 'react';
import ReactDom from 'react-dom';

export default class Loader extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		let classes = "loader";

		return (
			<div className={classes}>
				<div className="pacman"></div>
				<div className="dot"></div>
			</div>
		)
	}
}