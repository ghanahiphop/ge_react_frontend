import React from 'react';
import ReactDom from 'react-dom';
import VideoThumbnail from "./videoThumbnail.jsx";


export default class VideoList extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		let lists = this.props.data.map((item, index) => {
			return (
			  	<div className="col-md-3 col-sm-4 col-xs-6" key={index}>	
					<VideoThumbnail data={item} size={'default'}/>
				</div>
			)
		});

		if (lists.length === 0) {
			lists = (<div className="no-result">There is no video</div>);
		}
		
		return (
			<div className="row video-list">
				{lists}
			</div>
		);
	}
}
