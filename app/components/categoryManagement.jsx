import React from 'react';
import ReactDom from 'react-dom';
import Menu from "./menu.jsx";

export default class CategoryManagement extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			categoryChecked: false
		}
		
		this.classNames = require('classnames');
		this._ = require("underscore");
		this.categoryService = require("../services/category.js");
	}

	componentDidMount() {
	}

	componentWillReceiveProps(nextProps) {
		this.bindChangeEvents();
	}

	bindChangeEvents() {
		setTimeout(() => {
			jQuery('[name="parentCategory"]').change((e) => {
				if (jQuery('[name="parentCategory"]:checked').length >= 1) {
					this.setState({categoryChecked: true});
					this.activeCatetory = this.getCheckedCategory();
				}
			});
		})
	}

	inputOnChange(e) {
		if (e.target.value.length > 0) {
			this.setState({categoryChecked: true});
		}else if(!this.activeCatetory){
			this.setState({categoryChecked: false});
		}
	}

	validate() {
		if (!this.activeCatetory) {
			throw new Error("please select a category!");
		}

		return true;
	}

	addCategory() {
		console.info("--- add category ---");
		var categoryDom = jQuery(this.refs.newCategory);

		if (categoryDom.val().length <=0) {
			categoryDom.addClass("error");
			
			categoryDom.on("focus", () => {
				categoryDom.removeClass("error");
			});
		}else{
			this.categoryService.addCategory(categoryDom.val(), this.activeCatetory, (result) => {
				this.props.categoryChange(result);
			});
		}
	}

	editCategory() {
		this.validate();
		this.refs.editCategory.value = this.activeCatetory.title;
		jQuery("#editModal").modal("show");
	}

	saveChanges() {
		this.activeCatetory.title = this.refs.editCategory.value;
		
		this.categoryService.updateCategory(this.activeCatetory, (result) => {
			this.props.categoryChange(result);
			jQuery("#editModal").modal("hide");
		});
	}

	deleteCategory() {
		this.validate();

		if (confirm("are you sure you want to delete category '" + this.activeCatetory.title + "'?")) {
			console.info("---deleteCategory", this.activeCatetory);
			this.categoryService.deleteCategory(this.activeCatetory, (result) => {
				this.props.categoryChange(result);
			})
		}
	}

	getCheckedCategory() {
		let checkedId = jQuery('[name="parentCategory"]:checked').val();
		
		return this.categoryService.getCategoryById(this.props.data, checkedId);
	}

	render() {
		let controlPannelName = this.classNames("control-pannel", {'hide': !this.state.categoryChecked})

		return (
		    <div className="category-management"> 
			     <input type="text" name="newCategory" placeholder="new category" ref="newCategory" className="form-control" onChange={this.inputOnChange.bind(this)}/>
			     <label className="title">please select the parent category</label>
			     <Menu wichCheckbox='true' checkboxName="parentCategory" data={this.props.data}/>

			     <div className={controlPannelName}>
			     	<div className="modal fade" id="editModal" role="dialog">
			     		<div className="modal-dialog" role="document">
			     			<div className="modal-content">
			     				<div className="modal-header">
			     					 <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>	
			     					 <h4 className="modal-title">Edit Category</h4>
			     				</div>
			     				<div className="modal-body">
			     					<div>
			     						<label>Category Name</label>
			     						<input type="text" className="form-control" ref="editCategory"/>
			     					</div>
			     				</div>
			     				<div className="modal-footer">
							        <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
							        <button type="button" className="btn btn-primary" onClick={this.saveChanges.bind(this)}>Save changes</button>
							    </div>
			     			</div>
			     		</div>
			     	</div>


			     	<div className="btn-group" role="group">
			     		<div className="btn btn-success" onClick={this.addCategory.bind(this)}> 			     	
			     			<span className="glyphicon glyphicon-floppy-saved"></span>
			     		</div>

			     		<div className="btn btn-info" onClick={this.editCategory.bind(this)}> 			     	
			     			<span className="glyphicon glyphicon-edit"></span>
			     		</div>
			     		<div className="btn btn-danger" onClick={this.deleteCategory.bind(this)}>
			     			<span className="glyphicon glyphicon-floppy-remove"></span>
			     		</div>
			     	</div>
			     </div>
		    </div>
		)
	}
}