import React from 'react';
import ReactDom from 'react-dom';
import {Link} from 'react-router';

export default class Menu extends React.Component {
	constructor(props) {
		super(props);
	}

	getChildrenMenu(data) {
		var that = this,
			lis =  "";
		
		lis = data.map(function(d, k) {
			let children = <div></div>,
				item = null;

			if (d.children && d.children.length > 0) {
				children = that.getChildrenMenu(d.children);
			};

			if(that.props.wichCheckbox) {
				item = (<div><input type="radio" name={that.props.checkboxName} value={d.id}/>
					<label>{d.title}</label></div>);

			}else if(d.url && d.url!="") {
				item = (<Link to={d.url}>{d.title}</Link>);
			}else{
				item = (<a href="javascript:void(0)">{d.title}</a>);
			}
			
			return (<li key={k || d.url}>
					{item}
					{children}
				</li>);
		});

		return (<ul>{lis}</ul>);
	}

	render() {
		let menus = this.getChildrenMenu(this.props.data);
				
		return (<div>{menus}</div>);
	}
}