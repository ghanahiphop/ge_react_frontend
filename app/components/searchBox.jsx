import React from 'react';
import ReactDom from 'react-dom';

let _ = require("underscore")();

export default class SearchBox extends React.Component {
	constructor(props) {
		super(props);
		this.state = {value: ""};
	}

	handleSubmit(e) {
		if (e && e.preventDefault) {
			e.preventDefault();
		}
		
		if (this.props.cb && typeof _.isFunction(this.props.cb)) {
			this.props.cb(this.state.value);
		}
	}

	upateSearchQuery(e) {
		if (!e) return ;
		this.setState({value: e.target.value})
	}

	render() {
		return (
			<form onSubmit={this.handleSubmit.bind(this)}>
				<div className="search-box input-group">
					<input type="text" 
					  value={this.state.value}
					  placeholder="search"
					  onChange={this.upateSearchQuery.bind(this)} 
					  className="search-query form-control"/>
					<span className="input-group-btn">
						<button className="btn btn-primary" type="submit">
							<span className="glyphicon glyphicon-search"></span>
						</button>
					</span>
				</div>
			</form>
		);
	}
}
