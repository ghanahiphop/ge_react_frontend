
import React from 'react';
import ReactDom from 'react-dom';


export default class Links extends React.Component {
 
  constructor(props) {
    super(props);
  }

  render() {
  	var contents = this.props.contents;
  	var links ="";

    var savedMsg = "";
    var saveButton="";

  	if(contents.success){
  		links = contents.urls.map(function(url, index){
  			var href = url.href,
  				  title = url.title ? url.title : url.href;
  			return <li key={index}><a href={href}> { title }</a> </li>;
  		}) 
  	}

    if(this.props.isSaved){
      savedMsg = <div> Saving successfully!!</div>
    }

    if(contents.urls && contents.urls.length > 0) {
      saveButton = <input type="button" onClick={this.props.saveToDb} value="Save To DB" />;
    }

    return (
      <div className="col-sm-12">
          <div>{saveButton} </div>
          <div>
            <ol>
              {links}
            </ol>	
          </div>

          <div> {savedMsg} </div>
      </div>
    );
  }
}
