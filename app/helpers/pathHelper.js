let user = require("../services/users");
let config = require("../config.js");

module.exports = {
	buildPathWithToken: function(path) {
		return "/" + path + "?token=" + user.getUserToken();
	},

	buildPath: function(path) {
		return "http://" + config.host + ":" + config.port + path;
	}
}