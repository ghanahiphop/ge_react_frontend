module.exports = {
	getFileExtension: function(fileName) {
		return fileName.split(".").pop();
	}
}