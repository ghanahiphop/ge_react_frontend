'use strict';

let DomControl = function(dom) {
	this.dom = dom;
};

let obj = {
	select: function(dom) {
		return new DomControl(dom)
	}
};

let _ = require("underscore");

DomControl.prototype = {
	validate: function(){
		if (!this.dom) {
			throw "please select dom first";
		}
	},

	getOriginalClasses: function() {
		return this.dom.className.split(" ");
	},

	arrayToClassName: function(originalClasses) {
		let newClasses = originalClasses.join(" ");
		this.dom.className = newClasses;
	},

	addClass: function(className) {
		this.validate();

		let originalClasses = this.getOriginalClasses();
		
		originalClasses.push(className);

		this.arrayToClassName(originalClasses);

		return this;
	},

	removeClass: function(className) {
		this.validate();
		
		let originalClasses = this.getOriginalClasses(),
			pos = originalClasses.indexOf(className);
		
		originalClasses.splice(pos,1);

		this.arrayToClassName(originalClasses);
		
		return this;
	},

	hasClass: function(className) {
		let originalClasses = this.getOriginalClasses(),
			pos = originalClasses.indexOf(className);

		return pos != -1;
	},

	toggleClass: function(className) {
		if (this.hasClass(className)) {
			this.removeClass(className);
		}else{
			this.addClass(className);
		}
	},

	append: function(htmlStr) {
		this.dom.innerHTML = this.dom.innerHTML + htmlStr;

		return this;
	},

	isClass: function(selector) {
		return selector.charAt(0) === ".";
	},

	isId: function(selector) {
		return selector.charAt(0) === "#";
	},

	isTag: function(selector) {
		return /^[a-z|A-Z]+$/.test(selector);
	},

	bind: function(eventName, cb) {
		this.dom.addEventListener(eventName, cb);
	},

	css: function(cssObj) {
		_.each(cssObj, (v, k) => {
			this.dom.style[this.getCssKeyName(k)] = v;
		});
	},

	getCssKeyName: function(cssKey) {
		let nameArr = cssKey.split("-");

		if (nameArr.length <= 1) {
			return cssKey;
		}else{
			for (var i = 1; i < nameArr.length; i++) {
				nameArr[i] = nameArr[i].charAt(0).toUpperCase() + nameArr[i].substr(1);
			}

			return nameArr.join("");
		}
	},

	find: function(selector) {
		var dom = null;
		
		if (this.isClass(selector)) {
			dom = this.dom.getElementsByClassName(selector.substr(1))[0];
		}else if(this.isId(selector)) {
			dom = document.getElementById(selector.substr(1));
		}else if(this.isTag(selector)){
			dom = this.dom.getElementsByTagName(selector)[0];
		}

		return obj.select(dom);
	},

	get: function() {
		return this.dom;
	}
}


module.exports = obj;