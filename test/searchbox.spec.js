import React from 'react';
import { mount, shallow } from 'enzyme';
import {expect} from 'chai';
import SearchBox from "../app/components/SearchBox";

describe('<SearchBox/>', function () {
  let sinon = require("sinon");
  let cb = sinon.spy();

  const wrapper = shallow(<SearchBox cb={cb}/>);

  
  it('should have text input', function () {
    expect(wrapper.find('input')).to.have.length(1);
  });

  it("should have submit button", function() {
  	expect(wrapper.find("button")).to.have.length(1);
  });

  it("when button click, call back function shoudl be called", function() {
  	expect(cb.called).to.equal(false);
  	wrapper.find('form').simulate('submit');
  	expect(cb.calledWith("")).to.equal(true);
  });
});