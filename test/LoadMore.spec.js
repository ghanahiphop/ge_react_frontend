import React from 'react';
import { mount, shallow } from 'enzyme';
import {expect} from 'chai';
import LoadMore from "../app/components/loadMore.jsx";

describe('<LoadMore/>', function () {
  let sinon, cb, isLoading, wrapper;

  beforeEach(function() {
      sinon = require("sinon");
      cb = sinon.spy();
      isLoading = true;
      wrapper = shallow(<LoadMore cb={cb} loading={isLoading}/>);
  });


  it('should have btn', function () {
      expect(wrapper.find('.btn')).to.have.length(1);
  });

  it("when button click, call back function shoudl be called", function() {
    	expect(cb.called).to.equal(false);
    	wrapper.find('.btn').simulate('click');
    	expect(cb.called).to.equal(true);
  });

  it("when it has props loading and it's value is true", function() {
      expect(wrapper.find(".load-more")).to.have.length(1);
      expect(wrapper.find(".loading")).to.have.length(1);
  });

  it("when the value of isLoading is false, it should not have .loading class", function() {
      isLoading = false;
      wrapper = shallow(<LoadMore cb={cb} loading={isLoading}/>);
      expect(wrapper.find(".loading")).to.have.length(0);
  });

  it("when the value of display is false, it should have hide class", function() {
      var display = false;
      wrapper = shallow(<LoadMore cb={cb} loading={isLoading} display={display}/>);
      expect(wrapper.find(".hide")).to.have.length(1);
  });

});