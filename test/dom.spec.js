import {expect} from 'chai';

describe('helper dom.js', function () {
  let domHelper = require("../app/helpers/dom.js");
  let testDom =  domHelper.select(document.createElement("div"));

  it("test getCssKeyName", function() {
  	   expect(testDom.getCssKeyName("min-width")).to.equal("minWidth");
  	   expect(testDom.getCssKeyName("min-height")).to.equal("minHeight");
  });
});