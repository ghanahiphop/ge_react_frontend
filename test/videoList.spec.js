import React from 'react';
import { mount, shallow } from 'enzyme';
import {expect} from 'chai';
import VideoList from "../app/components/videoList.jsx";

describe('<VideoList/>', function () {
  it('no result should display', function () {
      let items = [];
      let wrapper = shallow(<VideoList data={items}/>);
      expect(wrapper.find('.no-result')).to.have.length(1);
  });

  it("should have submit button", function() {
      let items = [1, 2, 3];
      let wrapper = shallow(<VideoList data={items}/>);
      expect(wrapper.find('.no-result')).to.have.length(0);
  });
});