import React from 'react';
import { mount, shallow } from 'enzyme';
import {expect} from 'chai';
import YoutubeVideo from "../app/components/youtubeVideo";

describe('<YoutubeVideo/>', function () {
  const wrapper = mount(<YoutubeVideo videoId={'test'}/>);

  it("test setVideoSize", function() {
      var instance = wrapper.component.getInstance();
      instance.setVideoSize(60);
      expect(instance.state.width).to.equal(60);
      expect(instance.state.height).to.equal(40);
  });
});