import React from 'react';
import { mount, shallow } from 'enzyme';
import {expect} from 'chai';
import OverlayProgress from "../app/components/overlayProgress.jsx";

describe('<OverlayProgress/>', function () {
  let sinon, cb, isLoading;

  beforeEach(function() {
      isLoading = true;
  });

  it("test dom percentage value when propos is 50", () => {
      const wrapper = shallow(<OverlayProgress percentage="50"/>);
      expect(wrapper.find('.progress')).to.have.length(1);
      expect(wrapper.find('.progress-bar')).to.have.length(1);
      expect(wrapper.find('.progress-bar').node.props["aria-valuenow"]).to.equal("50");
      expect(wrapper.find('.progress-bar').node.props["style"].width).to.equal('50%');
  });

  it("test dom percentage value when propos is 80", () => {
      const wrapper = shallow(<OverlayProgress percentage="80"/>);
      expect(wrapper.find('.progress')).to.have.length(1);
      expect(wrapper.find('.progress-bar')).to.have.length(1);
      expect(wrapper.find('.progress-bar').node.props["aria-valuenow"]).to.equal("80");
      expect(wrapper.find('.progress-bar').node.props["style"].width).to.equal('80%');
  });
});