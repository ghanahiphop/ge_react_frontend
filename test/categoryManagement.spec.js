import React from 'react';
import { mount, shallow } from 'enzyme';
import {expect} from 'chai';
import CategoryManagement from "../app/components/categoryManagement";

describe('<CategoryManagement/>', function () {
  var data = [
  	  {
	  	  id: 1,
	  	  title: "test1"
  	  },
  	  {
  	  	id: 2,
  	  	title: "test2"
  	  }
  ];

  const wrapper = mount(<CategoryManagement data={data}/>);

  it("function: getCategoryById", function() {
      var instance = wrapper.component.getInstance();
      var result = instance.categoryService.getCategoryById(data, 1);

      expect(result).to.equal(data[0]);

      var testData = [
      	 {
      	 	id: 1,
      	 	title: "test1",
      	 	children: [{
      	 		id: 2,
      	 		title: "test2"
      	 	}]
      	 }
      ];

      var result = instance.categoryService.getCategoryById(testData, 2);

      expect(result).to.equal(testData[0].children[0]);

      var testData = [
      	 {
      	 	id: 1,
      	 	title: "test1",
      	 	children: [{
      	 		id: 2,
      	 		title: "test2",
      	 		children: [
      	 			{
      	 				id:3,
      	 				title:"test3"
      	 			}
      	 		]
      	 	}]
      	 }
      ];

      var result = instance.categoryService.getCategoryById(testData, 3);

      expect(result).to.equal(testData[0].children[0].children[0]);

  });
});